RFID Bridge project

General Design
———————————————————————————————————————————————————————————————————————————————
RFID board:  RFIDRW-E-TTL from http://www.priority1design.com.au

Antenna coil: Comes with the RFID board

Arduino: Mega 
Can be run on Uno, but Mega simplifies things. Specifically, the Mega has more room for program code and variables, and it has multiple serial ports. The Uno has only one serial port over which the RFID board and the driving program must talk. Interference between the two lines of communication is eliminated if we have two distinct lines, as on the Mega.

Software:
The Arduino software is designed to do two things:

1) listen for commands coming from a driver program and take action based on those commands.

2) listen for strings coming from the RFID board, and relay those strings back to the driver program.

The Arduino is tethered to the computer (via a serial line) that runs the driver program. As an example, I’ve included a driver program written in Python, but the driver could be written in other languages.



RFID board connections
———————————————————————————————————————————————————————————————————————————————
A+
A-  These pins are connected to either end of the antenna coil. See “Tuning the Antenna” below for adjustment information.

RFID Pin —> Pin on Mega
   RX    —> TX1
   TX    -> RX1
   V+    -> 5V 
   V-    -> GND

(This is optional)
   L+    -> 1k Ohms resister -> LED -> GRD (LED lights when RFID tag is detected)


Using the RFID board and Arduino
———————————————————————————————————————————————————————————————————————————————
1. Connect the RFID board as above.
2. Connect the Arduino to the computer using a serial line.
3. Open the rfidBridge sketch in the Arduino IDE and upload it to the board.
4. Run the rfidDriver.py program in a terminal. (You may need to update the serial port designation for the computer you’re using.)
5. Type ‘commands’ in the Terminal to see what is available.
6. If this is the first run, you may need to tune the antenna before the board will detect tags. See Tuning the Antenna below.


Tuning the Antenna
———————————————————————————————————————————————————————————————————————————————
The antenna’s frequency should be 134 kHz to work with the mouse RFID tags. The resonant frequency of the antenna is determined by the number of coils of wire in the loop _and_ by the length of the leads connecting the antenna wire to the RFID board. By issuing the ‘FREQ’ (or the synonym ‘MOF’) command in the driver program, you can find out the current frequency of the antenna. Here’s what Robert Accardi of Priority 1 Design said about adjusting the frequency:

——
When attaching a cable I use a simple figure 8 pair. Do not use co-axial or anything shielded.  The frequency of the antenna will still drop, but you can adjust it in one of two ways.  Add a capacitor (about 10nf at first) in series with the antenna cable at the L- terminal. This will bring the frequency back up. Use the MOF command to check and adjust as needed.
Alternatively you can simply pull turns off the antenna. It should peel off ok but be careful as those wires are fragile. check the frequency as you remove turns. Keep your hands off the antenna while checking it as you won't get a proper reading that way.
—
