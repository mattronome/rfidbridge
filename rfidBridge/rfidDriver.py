#!/usr/bin/python

import serial
import time
import sys
from select import select

connected = False

# change serial port designation as needed for the host computer you're using
ser = serial.Serial("/dev/tty.usbmodemfd131", 9600)

while not connected:
    serin = ser.read()
    connected = True
    
print "rfidDriver.py: Connected to Arduino board. Enter 'commands' for list."

# act on data coming from Arduino. Mostly "OK" or tag ID numbers

def actOnSerialString(serialString):
    print serialString
    
    # demonstration of acting on a RFID tag
    if serialString == 'RFID board: 0C00F80060':
        print "Hen 0C00F80060 detected, setting pin 13 high for 3 secs."
        ser.write("setPin, 13, HIGH, 3000")
    elif serialString == 'RFID board: 900_110000042324':
        print "Mouse 900_110000042324 detected, setting pin 13 high for 5 secs."
        ser.write("setPin, 13, HIGH, 5000")


inputs = [ sys.stdin, ser ] # fds from which we expect to read
outputs = [ ] # fds to which we expect to write
timeout = 10
def loop():
    readable, writable, exceptional = select(inputs, outputs, inputs, timeout)
    
    for s in readable:
        if s is sys.stdin:
            inputString = sys.stdin.readline().strip()
            
            if inputString == 'exit':
                ser.close()
                sys.stdin.close()
                print "     ...good bye."
                exit()
            else:
                # send the string to the device
                ser.write(inputString)
    
        if s is ser:
            outputString = ser.readline().strip()
            actOnSerialString(outputString)


while(True):
    loop()