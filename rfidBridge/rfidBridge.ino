/*
  rfidBridge
  
  
*/
#include "Timer.h"  // see http://playground.arduino.cc/Code/Timer for info

Timer t;

// Status variables
String tagType = "SD0"; // "SD2" for FDX-B/HDX (mouse) type. "SD0" is for chicken tags.
boolean readyState = false;
int baudRate = 9600;

void displayStatus() {
  String isReady = readyState ? "Yes" : "No" ;
  String tagString;
  
  if (tagType.equals("SD0")) {
    tagString = tagType + " (for chickens)";
  } else if (tagType.equals("SD2")) {
    tagString = tagType + " (for mice)";
  } else {
    tagString = tagType;
  }

  printString("====================== Arduino Status =====================", "");
  printString("ready: ", isReady);
  printString("tagType: ", tagString);
  printString("baud rate: ", String(baudRate));
  printString("Free SRAM: ", String(freeRam(), DEC));
}

void displayCommands() {
  Serial.println(F("====================== Command List ======================="));
  Serial.println(F("commands"));
  Serial.println(F("SD0 (for chicken tags)"));
  Serial.println(F("SD2 (for mouse tags, default)"));
  Serial.println(F("setPinToStateForDuration,(pinNum), (HIGH/LOW), (millisecs, or -1 for forever) -- 'setPin' is abbreviation."));
  Serial.println(F("freq (returns resonant frequency of attached antenna)"));
  Serial.println(F("status"));
  Serial.println(F("exit"));
}

void setTagType(String type) {
  tagType = type;
  Serial1.println(tagType);
  Serial1.flush();
}

void setPinState(int pin, String pinState, int duration) {
  pinMode(pin, OUTPUT);
  uint8_t newState = pinState.equals("HIGH") ? HIGH : LOW;
  
  if (duration == -1) {
    digitalWrite(pin, newState);
  } else {
    t.pulseImmediate(pin, duration, newState);
  }
  
}

void setPinWithString(String line) {
  int firstCommaIndex = line.indexOf(',');
  int secondCommaIndex = line.indexOf(',', firstCommaIndex+1);
  int thirdCommaIndex = line.indexOf(',', secondCommaIndex+1);

  String pinNumString = line.substring(firstCommaIndex+1, secondCommaIndex);
  pinNumString.trim();
  int pinNum = pinNumString.toInt();  // TODO: sanity check this before continuing
  
  String stateString = line.substring(secondCommaIndex+1, thirdCommaIndex);
  stateString.trim();
  
  String durationString = line.substring(thirdCommaIndex+1);
  durationString.trim();
  int duration = durationString.toInt();  // if toInt() fails, it returns zero

  setPinState(pinNum, stateString, duration);
  Serial.println(F("OK"));
}

void setup() {
  // Open serial connection
  Serial.begin(baudRate);
  
  while (!Serial) {
    ; // wait for serial port to connect. Needed for Leonardo only
  }

  // Start off with board-mounted LED at pin 13 off
  pinMode(13, OUTPUT);
  digitalWrite(13, LOW);
  
  // set up serial port communication with RFID board
  Serial1.begin(9600); 
  Serial1.println("SD0"); // "SD2" ensures that we're using FDX-B/HDX (mouse) type. "SD0" is for chicken tags.
  Serial1.flush();

  Serial.write("1\n"); // let python program know we're ready
  // send RFID board the SD2 command to set default state
  readyState = true;
}

void loop() {
  
  if (Serial.available()) {  // chars incoming from driver program/monitor
    String line;
   
    while (Serial.available()) {
      line = Serial.readStringUntil('\n');
    }
    line.trim(); // remove trailing whitespace
    line.toLowerCase();
    
    // command lines like: setPinToStateForDuration,13,HIGH,2000 (that is set pin 13 high for 2000 milliseconds) -- setPin is the abbreviation           
    if ((line.startsWith("setpin,")) || (line.startsWith("setpintostateforduration"))) {  
      setPinWithString(line);
    } else if (line.equals("sd0")) {
      setTagType("SD0");
      displayStatus();
    } else if (line.equals("sd2")) {
      setTagType("SD2");
      displayStatus();
    } else if (line.equals("freq") || line.equals("mof")) {  // pass this along to the RFID board
      Serial1.print("MOF\r");
    } else if (line.equals("status")) {
      displayStatus();
    } else if (line.equals("commands")) {
      displayCommands();
    } else if (line.equals("exit")){
      // Serial.println("Closing port and exiting");
      Serial.end();
    } else {
      printString("Unknown command: ", line);
    }
  }
  
  if (Serial1.available()) {  // chars incoming from RFID board
    String line;
    char charBuf[100];
   
    while (Serial1.available()) {
      line = Serial1.readStringUntil('\n');
    }
    line.trim(); // remove trailing whitespace
    
    printString("RFID board: ", line);
    Serial.write("\n");
  }
  
  t.update();  // let the timer know that time has passed.
}

// Diagnostics

// We only have 2K RAM to play with, and overstepping that fails in unpredictable ways
int freeRam() {
  extern int __heap_start, *__brkval; 
  int v; 
  return (int) &v - (__brkval == 0 ? (int) &__heap_start : (int) __brkval); 
}

// utilities
void printString(String prefix, String stringObj) {
  String outputString;  

  if (prefix.length() > 0) {
    outputString = prefix + stringObj;
  } else {
    outputString = stringObj;
  }
  outputString.trim();
  
  int charBufLen = outputString.length() + 1; // add room for trailing null character
  char charBuf[charBufLen];
  outputString.toCharArray(charBuf, charBufLen);

  Serial.print(charBuf);
  Serial.println(F("\r"));
}


